const fizzBuzz = (input) => {
  let result = "";
  for (let i = 1; i <= input; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
      result += "fizzBuzz ";
    } else if (i % 5 == 0) {
      result += "buzz ";
    } else if (i % 3 == 0) {
      result += "fizz ";
    } else {
      result += i + " ";
    }
  }

  return result;
};

const input = 25;

console.log(fizzBuzz(input));
