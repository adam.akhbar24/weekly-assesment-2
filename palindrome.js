const palindrome = (str) => {
  let reverse = "";

  // i = 2
  for (let i = str.length - 1; i >= 0; i--) {
    reverse += str[i];
  }
  if (reverse !== str) {
    return `false (${str} is not match with ${reverse})`;
  } else {
    return true;
  }

  return reverse === str; //`${str} is not match with ${reverse}`;
};

console.log(palindrome("kasur rusak"));
