const theAverage = (angka) => {
  // Receiving the input as angka
  let total = 0; // kita berikan variabel kosong untuk tempat penjumlahan
  for (let i = 0; i < angka.length; i++) {
    // disini kita berbicara tetukan nilai dari index, nilai index adalah nilai mulai
    total += angka[i]; //kita menjumlah kan semuanya sampai itemmnya habis, nah hasil penjumlahan yangtadi telah terkumpul di variabel total
  }

  return total / angka.length; // total dari penjumlahan bisa langsung dibagi
  //semuanya hasil penjumlahan dibagi jumlah item  (length)
};

const input = [9, 10, 3, 20, 7];

// passing data
console.log(theAverage(input));
