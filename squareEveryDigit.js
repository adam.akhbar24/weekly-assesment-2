const squareEveryDigit = (num) => {
  let output = num
    .split("")
    .map((el) => Math.pow(el, 2))
    .join("");

  return output;
};

const input = "2213";

console.log(squareEveryDigit(input));
