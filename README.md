# Weekly Assessment 2 - FE

## OddEven

> Answer the questions below in `oddEven.js`.

Create a function that will determined input number is even or odd number!

**Example**

```
Input: 3
Output: 3 is odd number

Input: 10
Output: 10 is even number

Input: 5
Output: 5 is odd number

```

## FizzBuzz

> Answer the questions below in `fizzBuzz.js`.

Create a function that will print numbers horizontally with specific condition:

1. Start from number 1
2. If number can be divided by 3 change number to `Fizz`
3. If number can be divided by 5 change number to `Buzz`
4. If number can be divided by 3 and 5 change number to `FizzBuzz`

**Example**

```
Input: 16
Output: 1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16

Input: 25
Output: 1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz Fizz 22 23 Fizz Buzz
```

## FindMaxNumber

> Answer the questions below in `findMaxNumber.js`.

Create a function that will find the highest number in a collection of numbers from an array.

**Example**

```
Input: [8, 2, 5, 10, 18, 9, 15]
Output: 18

Input: [9, -2, -4, 8, 1]
Output: 9

Input: [-4, -8, -10, -11, -5]
Output: -4
```

## TheAverage

> Answer the questions below in `theAverage.js`.

Create a function that will find the average of a collection of numbers from an array.

**Example**

```
Input: [1, 2, 3, 4, 5]
Output: 3

Input: [9, 10, 3, 20, 7]
Output: 9.8
```

## Palindrome

> Answer the questions below in `palindrome.js`.

Create a function that will define the word and the reverse word is match.

Note: never mind the capital.

**Example**

```
Input: hello
Output: false (hello is not match with olleh)

Input: kasur rusak
Output: true

Input: madam
Output: true

Input: academy
Output: false (academy is not match with ymedaca)
```

## Count the Word

> Answer the questions below in `countWord.js`.

Create a function to return the amount of word there are in a string.

Note: never mind the capital.

**Example**

```
Word to count: "cat"
Input: "cat and dog. cat and sheep"
Output: 2

Word to count: "dog"
Input: "Dogdogcatdogsheep"
Output: 3

```

## Turn Chord to Jazz

> Answer the questions below in `jazzify.js`.

Create a function which concatenates the number 7 to the end of every chord in an array. Ignore all chords which already end with 7.

**Example**

```
Input: ["G", "Am", "C"]
Output: ["G7", "Am7", "C7"]

Input: ["F7", "A, "E7", "D"]
Output: ["F7", "A7", "E7", "D7"]

Input: []
Output: []

```

## Square Every Digit

> Answer the questions below in `squareEveryDigit.js`.

Create a function that squares every digit of a number.

**Example**

```
Input: 9119
Output: 811181

Input: 1234
Output: 14916

Input: 889910
Output: 6464818110

```

## Capital to Front

> Answer the questions below in `capitalToFront.js`.

Create a function that moves all capital letters to the front of a word.

**Example**

```
Input: "hApPy"
Output: "APhpy"

Input: "hEllO"
Output: "EOhll"

Input: "caPITal"
Output: "PITcaal"

```

## Temperature Converter

> Answer the questions below in `tempConverter.js`.

Create a function that converts Celsius to Fahrenheit and vice versa.

**Example**

```
Input: "35C"
Output: "95F"

Input: "19F"
Output: "-7C"

Input: 10
Output: "Error"

```
