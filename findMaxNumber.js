const findMaxNumber = (input) => {
  return Math.max(...input);
};

const input = [8, 2, 5, 10, 18, 9, 15];

console.log(findMaxNumber(input));

//====================================================================================================
// another way
const number = [8, 2, 5, 10, 18, 9, 15];

function maxNumb(num) {
  let highest = 0;

  for (let i = 0; i < num.length; i++) {
    // Jika item saat ini lebih besar dari nilai highest
    // maka reassign nilai highest dengan item tersebut.
    // Jika tidak lanjut ke item berikutnya
    if (num[i] > highest) {
      highest = num[i];
    }
  }
  return highest;
}

console.log(maxNumb(number));
