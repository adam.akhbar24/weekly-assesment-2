const oddEven = (input) => {
  let result = "";
  for (let index = 1; index <= input; index++) {
    if (index % 2 == 0) {
      result = "even number";
    } else {
      result = "odd number";
    }
  }
  return result;
};

const input = 9;

console.log(oddEven(input));
