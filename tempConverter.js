function toFahrenheit(c) {
  return (9 / 5) * c + 32;
}

function toCelcius(f) {
  return ((f - 32) * 5) / 9;
}

const tempConverter = (temperature) => {
  let filterOnlyNumber = temperature
    .split("")
    .map((el) => parseInt(el))
    .filter((el) => !Number.isNaN(el))
    .join("");

  if (temperature.includes("C")) {
    return `${Math.round(toFahrenheit(filterOnlyNumber))}F`;
  } else if (temperature.includes("F")) {
    return `${Math.round(toCelcius(filterOnlyNumber))}C`;
  } else {
    return "Error";
  }
};

const input = "19F";

console.log(tempConverter(input));
